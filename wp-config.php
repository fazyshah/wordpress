<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'wordpress');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'O5Y4]00(ZstcFe[9&(vf_(CgP#x>EUT~ZXUoQ>BSC1P?E^J6Ux3=[^e:ka/n|A)|');
define('SECURE_AUTH_KEY',  'R@ZerIR1La.;Hd{GfU5]~BL:0<h&+cm/:k246Ls.u@=y>HI|EYK,bHKtMT~hT7iF');
define('LOGGED_IN_KEY',    'mlR<7g|-U`Givl&QkG(wy/+0Ha(a.+ G*<W0tgU?V{!y=NWJd[(M^zeM4Y_hWb|(');
define('NONCE_KEY',        'o<G][o3d~p^@dV %MDIoSEDO]Z[d|M;;_f6VmYh~vt_s/hq}_1ID[>v1Tcu!ren|');
define('AUTH_SALT',        'A`J|$rGsNCj lvBLdRF5i2^fdd*4(hFN[oyzF[ghmCDQ0?CB|^=:[jLNb/4:#MW^');
define('SECURE_AUTH_SALT', 'dHS!*y`LH2yb.H6lTi*u&,$splu/K]SRd/`YRc6(-d?^M?pZ*Coep~l(<d!@){.A');
define('LOGGED_IN_SALT',   'G|yfimyrY}xA~zO^G;]}HyLJ^yElOzLWig>uWx?6_S}]C)s[{g{@D1TT*R1*xc38');
define('NONCE_SALT',       '[=3-3<|YS*W%E3~zlB&SJ<3t!BnvR;W=36,[/rk~`UjNR>NEvRulkYfUn77WJ2l^');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
